from django import forms

class OpinionForm(forms.Form):
    opini_form = forms.CharField(
        label="",
        widget=forms.TextInput(attrs={'style': 'width: 40%; height: 15vh; border-radius: 25px'})
    )