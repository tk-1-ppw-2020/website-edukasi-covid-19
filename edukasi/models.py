from django.db import models

class Opinion(models.Model):
    opini = models.CharField(max_length=150)
    date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.opini
