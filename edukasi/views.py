from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import OpinionForm
from .models import Opinion

def edukasi(request):
    form_opini = OpinionForm()

    if request.method == 'POST':
        form_opini = OpinionForm(request.POST)

        if form_opini.is_valid():
            data = form_opini.cleaned_data
            opinion = Opinion()
            opinion.opini_form = data['opini_form']

            Opinion.objects.create(
                opini = request.POST.get('opini', opinion.opini_form)
            )

            return HttpResponseRedirect('/edukasi')

    context = {'form':form_opini.as_table, 'opinions':Opinion.objects.filter().order_by('-date')[:5]}
    return render(request, 'edukasi/edukasi.html', context)
