from django.shortcuts import render, redirect
from django.db.models import Q
from .models import RumahSakit

# Create your views here.
def index(request):
	data = RumahSakit.objects.all()
	response = {'data':data}
	return render(request, 'rs_rujukan/rs_rujukan.html', response)