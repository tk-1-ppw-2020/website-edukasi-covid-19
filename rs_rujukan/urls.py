from django.contrib import admin
from django.urls import path
from rs_rujukan import views

app_name = "rs_rujukan"

urlpatterns = [
   	path('', views.index, name='index'),
]