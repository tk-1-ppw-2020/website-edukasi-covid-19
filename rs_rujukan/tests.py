from django.test import TestCase, Client
from django.apps import apps
from .models import RumahSakit
from .apps import RsRujukanConfig

# Create your tests here.
class rs_rujukanTest(TestCase):
#app	
	def test_apakah_aplikasi_ada(self):
		self.assertEqual(RsRujukanConfig.name, 'rs_rujukan')
		self.assertEqual(apps.get_app_config('rs_rujukan').name, 'rs_rujukan')
#url
	def test_apakah_url_rs_rujukan_ada(self):
		response = Client().get('/rs_rujukan/')
		self.assertEquals(response.status_code, 200)

#template
	def test_apakah_halaman_rs_rujukan_memiliki_template(self):
		response =  Client().get('/rs_rujukan/')
		self.assertTemplateUsed(response, 'rs_rujukan/rs_rujukan.html')

#model
	def test_apakah_objek_RumahSakit_apabila_dicetak_akan_mereturn_namanya(self):
		objek = RumahSakit.objects.create(nama="nama", provinsi="provinsi", 
			telepon="telepon", alamat="alamat")
		self.assertIn(str(objek), "nama")
