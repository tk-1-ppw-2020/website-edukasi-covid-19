from django import forms
from . import models


CHOICES = [(True, 'Ya'), (False, 'Tidak')]

class CreateGejala(forms.ModelForm):
    class Meta:
        model = models.Gejala
        # Apparently exclude exist
        exclude = ['score']
        labels = {
            'demam': 'Apakah anda mengalami demam dalam 14 hari kebelakang?',
            'batuk': 'Apakah anda sering batuk-batuk kering dalam 14 hari kebelakang?',
            'lelah': 'Apakah anda sering merasa lelah dalam 14 hari kebelakang?',
            'diare' : 'Apakah anda mengalami dieare dalam 14 hari kebelakang?',
            'sakit_tenggorokan': 'Apakah Tenggorokan anda sakit dalam 14 hari kebelakang?',
            'sakit_kepala': 'Apakah anda mengalami sakit kepala dalam 14 hari kebelakang?',
            'ruam': 'Apakah terdapat ruam pada kulit anda dalam 14 hari kebelakang?',
            'rasa_bau': 'Apakah anda mengalami kehilangan indra perasa dan penciuman?',
            'sakit_dada': 'Apakah anda mengalami sakit dada dalam 14 hari kebelakang?',
            'kesusahan_bernapas' : 'Apakah anda mengalami kesulitan bernapas dalam 14 hari kebelakang?',
        }
        widgets = {
            'demam': forms.RadioSelect(attrs={'required': True}, choices=CHOICES),
            'batuk': forms.RadioSelect(attrs={'required': True}, choices=CHOICES),
            'lelah': forms.RadioSelect(attrs={'required': True}, choices=CHOICES),
            'diare' : forms.RadioSelect(attrs={'required': True}, choices=CHOICES),
            'sakit_tenggorokan': forms.RadioSelect(attrs={'required': True}, choices=CHOICES),
            'sakit_kepala': forms.RadioSelect(attrs={'required': True}, choices=CHOICES),
            'ruam': forms.RadioSelect(attrs={'required': True}, choices=CHOICES),
            'rasa_bau': forms.RadioSelect(attrs={'required': True}, choices=CHOICES),
            'sakit_dada': forms.RadioSelect(attrs={'required': True}, choices=CHOICES),
            'kesusahan_bernapas' : forms.RadioSelect(attrs={'required': True}, choices=CHOICES),
        }