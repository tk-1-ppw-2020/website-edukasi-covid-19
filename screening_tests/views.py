from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseNotFound
from .models import *
from .forms import *


def test(request):
    Gejala.objects.all().delete()
    form = CreateGejala
    if request.method == 'POST':
        form = CreateGejala(request.POST)

        if form.is_valid():
            form.save()

            return redirect('screening_tests:res')

    context = {
        'form_gejala': form,
    }
    return render(request, 'screening_tests/test.html', context)

def result(request):
    if Gejala.objects.all().count() != 0:
        gejala = Gejala.objects.latest('id')
        if gejala.score > 60:
            risk_lv = "high-risk"
            card_bg = "bg-danger"
        elif gejala.score > 30:
            risk_lv = "med-risk"
            card_bg = "bg-warning"
        elif gejala.score > 0:
            risk_lv = "low-risk"
            card_bg = "bg-warning"
        else :
            risk_lv = "zero"
            card_bg = "bg-success"

        context = {
            'gejala': gejala,
            'risk_lv': risk_lv,
            'card_bg': card_bg
        }
        return render(request, 'screening_tests/result.html', context)
    else :
        return redirect('screening_tests:test')