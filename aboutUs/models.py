from django.db import models

# Create your models here.
class KritikSaran(models.Model):
    kritik_saran = models.CharField(max_length=64)
    date = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    def __str__(self):
        return self.kritik_saran