from django.test import TestCase, Client
from django.utils.timezone import make_aware
from django.forms.models import model_to_dict
from .models import COVIDStat
from django.urls import reverse
from datetime import datetime
from datetime import date
from datetime import time
from .views import access_db

# Create your tests here.
class appTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.index_url = reverse('covid_count:index')
        self.table_url = reverse('covid_count:table')

    def test_access_index(self):
        # dummy object just to get the numbers up
        COVIDStat.objects.all().delete()
        first_count = COVIDStat.objects.all().count()
        response = self.client.get(self.index_url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(first_count + 1, COVIDStat.objects.all().count())
        self.assertContains(response, 
        model_to_dict(COVIDStat.objects.latest('date_accessed'))['confirmed'], 
        html=True)
        
    def test_access_table(self):
        # magic number as a marker
        TEST_entry1 = COVIDStat.objects.create(
            date_accessed = make_aware(datetime.now()),
            confirmed = 414141
            )
        
        response = self.client.get(self.table_url)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, '414141', html=True)

class statsAccessTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.first_access_url = reverse('covid_count:add')

    def test_access_once(self):
        first_count = COVIDStat.objects.all().count()
        response = self.client.get(self.first_access_url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(first_count + 1, COVIDStat.objects.all().count())

    def test_no_change(self):
        # both will have all non-date values zero
        TEST_entry1 = COVIDStat.objects.create(date_accessed = make_aware(datetime.now()))
        first_count = COVIDStat.objects.all().count()
        TEST_entry2 = COVIDStat(date_accessed = make_aware(datetime.now()))
        access_db(TEST_entry2)
        self.assertEqual(first_count, COVIDStat.objects.all().count())
    
    def test_with_change(self):
        # the latest entry is DEFINITELY not zero, considering the pandemic,
        # therefore, there must be change
        TEST_entry1 = COVIDStat.objects.create(date_accessed = make_aware(datetime.now()))
        first_count = COVIDStat.objects.all().count()
        response = self.client.get(self.first_access_url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(first_count + 1, COVIDStat.objects.all().count())
    
class statsModelTest(TestCase):
    def setUp(self):
        self.TEST_entry1 = COVIDStat.objects.create(date_accessed = make_aware(datetime.now()))
        self.TEST_entry2 = COVIDStat.objects.create(date_accessed = make_aware(datetime.now()))
        
    def test_create_stat(self):
        self.assertEqual(COVIDStat.objects.all().count(), 2)
    
    def test_stat_comparison(self):
        self.assertEqual(tuple(self.TEST_entry1.change(self.TEST_entry2).values()), (0,0,0,0))
    
