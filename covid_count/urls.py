from django.urls import path
from . import views

app_name = 'covid_count'

urlpatterns = [
    path('', views.index, name='index'),
    path('add/', views.access_db_and_return_latest, name='add'),
    path('table/', views.table, name='table')
]